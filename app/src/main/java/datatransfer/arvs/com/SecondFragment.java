package datatransfer.arvs.com;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {


    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_second, container, false);
        TextView txtv=view.findViewById(R.id.txtSecond);
        Bundle bundle=getArguments();
        Toast.makeText(getContext(), "Name"+bundle.getString("Name"), Toast.LENGTH_SHORT).show();
        String name=bundle.getString("Name")+" "+"second";
        txtv.setText(name);
        // Inflate the layout for this fragment
        return view;
    }

}

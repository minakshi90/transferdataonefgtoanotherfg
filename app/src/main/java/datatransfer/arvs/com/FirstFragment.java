package datatransfer.arvs.com;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {


    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_first, container, false);
       Button nextBtn= view.findViewById(R.id.next);

        nextBtn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                SecondFragment sf=new SecondFragment();
                Bundle b=new Bundle();
                b.putString("Name","Minakshi");
                sf.setArguments(b);
                getFragmentManager().beginTransaction().replace(R.id.firstFg,sf).commit();
               // getSupportFragmentManager().beginTransaction().add(R.id.).commit();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

}
